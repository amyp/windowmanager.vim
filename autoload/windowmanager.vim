" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100

let g:windowmanager_wraparound = get(g:, 'windowmanager_wraparound', v:false)

let s:cardinal = [ 'l', 'k', 'h', 'j' ]
function s:reverse_dir(dir)
  return xor(a:dir, 2)
endfunction

function! windowmanager#adjacent(dir, ...)
  let l:restore = get(a:000, 0, v:true)
  let l:relative = get(a:000, 1, v:false)

  let l:orig = winnr()
  execute 'wincmd' s:cardinal[a:dir]
  let l:ret = winnr()

  if l:ret == l:orig && l:relative && g:windowmanager_wraparound
    let l:move = 'wincmd '.s:cardinal[s:reverse_dir(a:dir)]
    let l:last = -1
    let l:ret = l:orig
    while l:last != l:ret
      execute l:move
      let l:last = l:ret
      let l:ret = winnr()
    endwhile
  endif

  if l:restore && l:ret != l:orig
    execute l:orig 'wincmd w'
  endif

  return l:orig != l:ret ? l:ret : -1
endfunction

function! windowmanager#exchange(dstw)
  let l:srcw = winnr()
  let l:srcb = bufnr('%')

  execute a:dstw 'wincmd w'
  let l:dstb = bufnr('%')
  execute 'hide buf' l:srcb

  execute l:srcw 'wincmd w'
  execute 'hide buf' l:dstb

  execute a:dstw 'wincmd w'
endfunction

function! windowmanager#move_to(dir)
  let l:nr = windowmanager#adjacent(a:dir, v:true, v:true)
  if l:nr != -1
    call windowmanager#exchange(l:nr)
  endif
endfunction

function! windowmanager#focus_to(dir)
  call windowmanager#adjacent(a:dir, v:false, v:true)
endfunction

function! s:move_edge(dir, push)
  let l:rmt = a:dir == 1 || a:dir == 2
  let l:win = l:rmt ? winnr() : -1
  let l:adj = windowmanager#adjacent(a:dir, !l:rmt)
  if l:adj != -1
    let l:vert = (a:dir == 0 || a:dir == 2) ? 'vertical' : ''
    let l:sign = (!a:push == l:rmt) ? '+' : '-'
    execute l:vert 'resize' l:sign.v:count1
    if l:win != -1
      execute l:win 'wincmd w'
    endif
  endif
endfunction

function! windowmanager#push_edge(dir)
  call s:move_edge(a:dir, v:true)
endfunction

function! windowmanager#pull_edge(dir)
  call s:move_edge(a:dir, v:false)
endfunction

" FIXME: would grabbing the buffers themselves feel better?
let s:win_stack = []
function! windowmanager#grab_win() abort
  call add(s:win_stack, win_getid())
endfunction

function! windowmanager#ungrab_win() abort
  while !empty(s:win_stack)
    let l:win = remove(s:win_stack, -1)
    let l:buf = winbufnr(l:win)
    if l:buf != -1
      return [l:win, l:buf]
    endif
  endwhile
  " Return the current window/buffer so callers become no-ops.
  return [win_getid(), bufnr('%')]
endfunction

function! windowmanager#ungrab_all() abort
  let s:win_stack = []
endfunction

function! windowmanager#place_win() abort
  execute 'buffer' windowmanager#ungrab_win()[1]
endfunction

function! windowmanager#swap_win() abort
  let l:cur = [win_getid(), bufnr('%')]
  let l:swp = windowmanager#ungrab_win()
  if l:cur != l:swp
    let l:save = [&hidden, &autowrite, &autowriteall]
    set hidden noautowrite noautowriteall
    call win_gotoid(l:swp[0])
    execute 'buffer' l:cur[1]
    call win_gotoid(l:cur[0])
    execute 'buffer' l:swp[1]
    let [&hidden, &autowrite, &autowriteall] = l:save
  endif
endfunction
