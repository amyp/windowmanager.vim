" windowmanager.vim: mappings for simple window management
" vim:fdm=marker:ts=2:sts=2:sw=2:et:tw=100
"
" Window management in vim is powerful but verbose.  For simple operations, mappings would be
" easier.  windowmanager.vim provides four sets of mappings, with directional variants for each.
"  <Plug>WindowManagerFocus...: focus the neighbor of the current window
"  <Plug>WindowManagerMove...: swap the current window's buffer with that of its neighbor
"  <Plug>WindowManagerPush...: push the specified border of this window out; count supported
"  <Plug>WindowManagerPull...: pull the specified border of this window in; count supported
"
" Directions used are Right, Left, Top, Bottom.  g:windowmanager_wraparound controls whether Focus
" and Move will wrap around to the other side of the screen.

if exists("g:windowmanager_loaded")
  finish
endif
let g:windowmanager_loaded = v:true

function! s:create_maps()
  let l:cardinal = [ 'Right', 'Top', 'Left', 'Bottom' ]
  for [l:op, l:fn] in [
        \ ['Move', 'move_to'],
        \ ['Focus', 'focus_to'],
        \ ['Push', 'push_edge'],
        \ ['Pull', 'pull_edge'],
        \ ]
    for l:i in range(4)
      execute 'nmap <silent> <Plug>(WindowManager'.l:op.l:cardinal[l:i].')'
            \ ':<C-U>call windowmanager#'.l:fn.'('.l:i.')<CR>'
    endfor
  endfor

  for [l:op, l:fn] in [
        \ ['Grab', 'grab_win'],
        \ ['Ungrab', 'ungrab_win'],
        \ ['UngrabAll', 'ungrab_all'],
        \ ['Place', 'place_win'],
        \ ['Swap', 'swap_win'],
        \ ]
    execute 'nmap <silent> <Plug>(WindowManager'.l:op.')'
          \ ':<C-U>call windowmanager#'.l:fn.'()<CR>'
  endfor
endfunction
call s:create_maps()
